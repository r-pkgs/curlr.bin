
# curlR.bin

> Binary files for curlR

## Windows

- `Java`: selenium需要`Java.exe`才能运行，windows用户可以通过360安装`Java(TM) 9 64位`；
  添加`C:\Program Files\Java\jre-9.0.1\bin`到环境变量`PATH`。

- `chromium`: <https://chromium.woolyss.com/>

- `chromedriver` (v107): <https://registry.npmmirror.com/binary.html?path=chromedriver/>, <https://chromedriver.chromium.org/>

- `firefoxdriver` (v0.30.0): <https://www.selenium.dev/downloads/>

- `selenium`: <https://www.selenium.dev/downloads/>

> `chromium`和`chromedriver`的版本号要一字不差的对应起来！

## Linux

```bash
sudo apt install firefox-geckodriver chromium-chromedriver 
```
